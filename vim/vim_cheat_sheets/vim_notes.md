## Vim Notes
### Direction Keys
| Command | Action |
|---------|--------|
| `H`     | Left   |
| `J`     | Down   |
| `K`     | Up     |
| `L`     | Right  |

### Text Editing
| Command  | Action                                                                 |
|----------|------------------------------------------------------------------------|
| `x`      | Delete character                                                       |
| `A`      | Append text to line                                                    |
| `a`      | Append to word                                                         |
| `dw`     | Delete word                                                            |
| `d$`     | Delete till end of line                                                |
| `dw`     | Delete until the start of the next word, EXCLUDING its first character |
| `de`     | Delete to the end of the current word, INCLUDING the last character    |
| `d$`     | Delete to the end of the line, INCLUDING the last character            |
| `dd`     | Delete line                                                            |
| `u`      | Undo                                                                   |
| `U`      | Return line to original state                                          |
| `Ctrl-R` | Redo                                                                   |
| `di"`    | Delete all in "" EXCLUDING ""                                          |
| `da"`    | Delete all in "" INCLUDING ""                                          |
| `rx`     | Replace char under cursor with 'x'                                     |
| `ce`     | Change until end of word                                               |
| `R`      | Replace mode                                                           |
| `y`      | Copy text                                                              |
| `p`      | Past text                                                              |
| `o`      | Open line below cursor                                                 |
| `O`      | Open line above cursor                                                 |

- Delete syntax: `d number motion`
- Change syntax: `c [number] motion`

### Moving around
| Command | Action                     |
|---------|----------------------------|
| `2w`    | Two words forward          |
| `3e`    | End of three words forward |
| `0`     | Start of line              |
| `G`     | Move to end of file        |
| `gg`    | Move to beginning of file  |
| `e`     | Move to end of word        |

### Searching
| Command  | Action                                         |
|----------|------------------------------------------------|
| `/`      | Search forward                                 |
| `?`      | Search backward                                |
| `n`      | Search again                                   |
| `N`      | Search in opposite direction                   |
| `Ctrl-O` | Go back where you came from                    |
| `Ctrl-I` | Go forward                                     |
| `%`      | Place on (, [, { then % to find matching ),],} |

### Substitute Command
| Command            | Action                            |
|--------------------|-----------------------------------|
| `:s/old/new/gc`    | Find and replace (global confirm) |
| `:#,#s/old/new/gc` | Find and replace between lines    |

### External Commands
| Command   | Action         |
|-----------|----------------|
| `:!ls`    | Run ls command |
| `:w FILE` | Write to file  |

### Merge Files
| Command   | Action                       |
|-----------|------------------------------|
| `:r FILE` | Marge FILE into current file |

### Splits
| Command            | Action                  |
|--------------------|-------------------------|
| `:sp filename`     | Horizontal split        |
| `:vsp filename`    | Vertical split          |
| `Ctrl-w H`         | Focus left              |
| `Ctrl-w J`         | Focus down              |
| `Ctrl-w K`         | Focus up                |
| `Ctrl-w L`         | Focus right             |
| `Ctrl-w n+`        | + size by n lines       |
| `Ctrl-w n-`        | - size by n lines       |
| `:resize n`        | change height to n rows |
| `:res +n`          | add n rows to split     |
| `:res -n`          | sub n rows to split     |
| `:vertical res +n` | add n cols to split     |
| `:vertical res -n` | sub n cols to split     |
| `Ctrl-w >`         | add 1 col to split      |
| `Ctrl-w <`         | sub 1 col to split      |
