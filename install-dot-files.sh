#!/bin/bash

VIM_DIR=$HOME/.vim
VUNDLE_DIR=$VIM_DIR/bundle/Vundle.vim

read -r -n 1 -p "[?] This will delete $HOME/[.vimrc .vim .bash_aliases .bash_functions]. Continue? [y/n] "
echo

if [[ ! $REPLY =~ ^[Yy]$ ]]
then
  echo "Abort.";
  exit 0
fi

# Move dotfiles to $HOME
declare -a DOT_FILES=("vim/vimrc" "bash/bash_aliases" "bash/bash_functions" "tmux/tmux.conf")
for DOT_FILE in "${DOT_FILES[@]}"
do
    BASE_NAME=$(basename "$DOT_FILE")
    cp $DOT_FILE "$HOME/.$BASE_NAME"
done

grep -q bash_functions $HOME/.bashrc

if [ $? -ne 0 ]
then
    echo """
if [ -f $HOME/.bash_functions ]; then
    . $HOME/.bash_functions
fi""" >> $HOME/.bashrc

fi

. $HOME/.bashrc

# Install required binaries
which vim > /dev/null 2>&1

if [ $? -ne 0 ]
then
    echo "[*] Installing vim"
    sudo apt install vim
    if [ $? -ne 0 ]
    then
        echo "[!] Failed to install vim"
        exit 1
    else
        echo "[+] Installed vim"
    fi
else
    echo "[*] Found vim"
fi

which git > /dev/null 2>&1

if [ $? -ne 0 ]
then
    echo "[*] Installing git"
    sudo apt install git
    if [ $? -ne 0 ]
    then
        echo "[!] Failed to install git"
        exit 1
    else
        echo "[+] Installed git"
    fi
else
    echo "[*] Found git"
fi

VIM_DIR=$HOME/.vim
if [ -d "$VIM_DIR" ]
then
    echo "[*] Removing old vim directory ($VIM_DIR)"
    sudo rm -r $VIM_DIR
    if [ $? -ne 0 ]
    then
        echo "[!] Failed to remove $VIM_DIR"
        exit 1
    else
        echo "[+] Removed $VIM_DIR"
    fi
    echo "[*] Creating vim directory ($VIM_DIR)"
    mkdir -p $VIM_DIR
    cp -r ./vim/* $VIM_DIR/
fi

if [ ! -d "$VUNDLE_DIR" ]
then
    echo "[*] Cloning Vundle"
    git clone https://github.com/VundleVim/Vundle.vim.git $VUNDLE_DIR > /dev/null 2>&1
    if [ $? -ne 0 ]
    then
        echo "[!] Failed to clone Vundle"
        exit 1
    else
        echo "[+] Cloned Vundle"
    fi
fi

echo "[*] Installing vim plugins"
# install plugins, quit vundle, quit vim
vim -c "PluginInstall" -c "q" -c "q"
if [ $? -ne 0 ]
then
    echo "[!] Failed to install vim plugins"
    exit 1
else
    echo "[+] Installed vim plugins"
fi


echo "[+] Done"
exit 0
